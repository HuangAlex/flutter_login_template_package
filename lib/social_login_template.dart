library social_login_template;

/// A Calculator.
// class Calculator {
//   /// Returns [value] plus 1.
//   int addOne(int value) => value + 1;
// }
export './src/widget/base_login.dart';
export './src/widget/base_login_phone.dart';
export './src/widget/base_login_social.dart';
export './src/widget/social_login_list.dart';
export './src/widget/email_login_widget/email_login.dart';
export './src/widget/email_login_widget/email_register.dart';
export './src/widget/phone_login_widget/login_phone_verification.dart';
export './src/auth/auth_helper.dart';

