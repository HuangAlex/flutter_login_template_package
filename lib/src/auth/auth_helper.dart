import 'dart:convert';
import 'dart:math';

import 'package:crypto/crypto.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';

class AuthHelper {
  static final _auth = FirebaseAuth.instance;

  static String _verificationId;

  static Future<String> signInWithPhoneNumber(String smsCode) async {
    try {
      final AuthCredential credential = PhoneAuthProvider.credential(
        verificationId: _verificationId,
        smsCode: smsCode,
      );

      final User user = (await _auth.signInWithCredential(credential)).user;

      print("Successfully signed in UID: ${user.uid}");
      return Future.value(user.uid);
    } catch (e) {
      print("Failed to sign in: " + e.toString());
      return Future.error(e.toString());
    }
  }

  static Future<String> verifyPhoneNumber(String phoneNumber) async {
    try {
      await _auth.verifyPhoneNumber(
          phoneNumber: phoneNumber,
          timeout: const Duration(seconds: 60),
          verificationCompleted: (PhoneAuthCredential credential) async {
            // ANDROID ONLY!
            // Sign the user in (or link) with the auto-generated credential
            // try {
            //   await _auth.signInWithCredential(credential);
            //   print(
            //       "Phone number automatically verified and user signed in: ${_auth.currentUser.uid}");
            //   return Future.value('done');
            // } catch (error) {
            //   return Future.error(error.toString());
            // }
          },
          verificationFailed: (FirebaseAuthException e) {
            // if (e.code == 'invalid-phone-number') {
            //   return Future.error('The provided phone number is not valid.');
            // }
            print(
                'Phone number verification failed. Code: ${e.code}. Message: ${e.message}');
            return Future.error(e.code);
          },
          codeSent: (String verificationId, int resendToken) async {
            // Update the UI - wait for the user to enter the SMS code
            _verificationId = verificationId;
          },
          codeAutoRetrievalTimeout: (String verificationId) {
            // Auto-resolution timed out...
            return Future.error('time-out');
          });
      return Future.value('done');
    } catch (e) {
      print("Failed to Verify Phone Number: $e");
      return Future.error(e.toString());
    }
  }

  /// Method for user login using [email] and [password]
  static Future<String> signInWithEmailAndPassword(
      String email, String password) async {
    try {
      UserCredential userCredential = await _auth.signInWithEmailAndPassword(
          email: email, password: password);

      if (userCredential.user.emailVerified) {
        return Future.value('done');
      } else {
        return Future.error('email-not-verified');
      }
    } on FirebaseAuthException catch (e) {
      return Future.error(e.message);
    } catch (error) {
      return Future.error(error);
    }
  }

  /// Method for new user registration using [email] and [password]
  static Future<String> registerWithEmailAndPassword(
      String email, String password) async {
    try {
      UserCredential userCredential = await _auth
          .createUserWithEmailAndPassword(email: email, password: password);
      sendVerificationEmail();
      return Future.value(userCredential.user.uid);
    } on FirebaseAuthException catch (e) {
      return Future.error(e.message);
    } catch (error) {
      return Future.error(error);
    }
  }

  /// Method to send verification email
  static Future<void> sendVerificationEmail() {
    return _auth.currentUser.sendEmailVerification();
  }

  /// Method to sign in with Google
  static Future<UserCredential> signInWithGoogle() async {
    // Trigger the authentication flow
    final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    // Create a new credential
    final GoogleAuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithCredential(credential);
  }

  /// Apple Sign in
  /// Generates a cryptographically secure random nonce, to be included in a
  /// credential request.
  static String generateNonce([int length = 32]) {
    final charset =
        '0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._';
    final random = Random.secure();
    return List.generate(length, (_) => charset[random.nextInt(charset.length)])
        .join();
  }

  /// Returns the sha256 hash of [input] in hex notation.
  static String sha256ofString(String input) {
    final bytes = utf8.encode(input);
    final digest = sha256.convert(bytes);
    return digest.toString();
  }

  /// Method to sign in with Apple
  static Future<UserCredential> signInWithApple() async {
    // To prevent replay attacks with the credential returned from Apple, we
    // include a nonce in the credential request. When signing in in with
    // Firebase, the nonce in the id token returned by Apple, is expected to
    // match the sha256 hash of `rawNonce`.
    final rawNonce = generateNonce();
    final nonce = sha256ofString(rawNonce);

    // Request credential for the currently signed in Apple account.
    final appleCredential = await SignInWithApple.getAppleIDCredential(
      scopes: [
        AppleIDAuthorizationScopes.email,
        AppleIDAuthorizationScopes.fullName,
      ],
      nonce: nonce,
    );

    // Create an `OAuthCredential` from the credential returned by Apple.
    final oauthCredential = OAuthProvider("apple.com").credential(
      idToken: appleCredential.identityToken,
      rawNonce: rawNonce,
    );

    // Sign in the user with Firebase. If the nonce we generated earlier does
    // not match the nonce in `appleCredential.identityToken`, sign in will fail.
    return await FirebaseAuth.instance.signInWithCredential(oauthCredential);
  }

  static Future<UserCredential> signInWithFacebook() async {
    // Trigger the sign-in flow
    final LoginResult accessToken = await FacebookAuth.instance.login();

    // Create a credential from the access token
    final FacebookAuthCredential facebookAuthCredential =
        FacebookAuthProvider.credential(accessToken.accessToken.token);

    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance
        .signInWithCredential(facebookAuthCredential);
  }
}
