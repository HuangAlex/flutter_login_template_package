import 'package:flutter/material.dart';

class MySocialWidgets extends StatefulWidget {
  final List<Widget> loginWidgets;
  final Axis scrollDir;
  final double width;
  final double height;
  MySocialWidgets({this.width,this.height,this.loginWidgets,this.scrollDir});
  @override
  _MySocialWidgetsState createState() => _MySocialWidgetsState();
}

class _MySocialWidgetsState extends State<MySocialWidgets> {
  List<Widget> _loginWidgets;
  Axis _scrollDir;
  double _width;
  double _height;
  @override
  void initState() {
    _loginWidgets = widget.loginWidgets;
    _scrollDir = widget.scrollDir;
    _width = widget.width;
    _height = widget.height;
    if(_loginWidgets == null){
      _loginWidgets = [];
    }
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      width: _width,
      height: _height,
      child:
        ListView.builder(
          scrollDirection: _scrollDir,
          padding: EdgeInsets.all(8),
          itemCount: _loginWidgets.length,
          itemBuilder: (BuildContext context, int index){
            return _loginWidgets[index];
          }
        )
    );
  }
}