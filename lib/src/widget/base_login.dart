import 'package:flutter/material.dart';

import 'base_login_phone.dart';
import 'base_login_social.dart';

class BaseLogin extends StatefulWidget {
  final Image logoImage;
  final DecorationImage backgroundImage;
  final Text logoTagLine;
  final Color backgroundColor;
  final PhoneNumberLogin phoneNumberLogin;
  final SocialLogin socialLogin;
  BaseLogin(
      {this.backgroundColor,
      this.backgroundImage,
      this.logoImage,
      this.logoTagLine,
      this.phoneNumberLogin,
      this.socialLogin});
  @override
  _BaseLoginState createState() => _BaseLoginState();

  static SnackBar showSnackBar(String title) {
    final snackbar = SnackBar(
      content: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 15),
      ),
    );
    return snackbar;
  }
}

//base class only have background and logo
class _BaseLoginState extends State<BaseLogin> {
  Image _logoImage;
  DecorationImage _backgroundImage;
  Text _logoTagLine;
  Color _backgroundColor;
  bool _showPhoneLogin;
  PhoneNumberLogin _phoneNumberLogin;
  SocialLogin _socialLogin;

  @override
  void initState() {
    this._logoImage = widget.logoImage;
    this._logoTagLine = widget.logoTagLine;
    this._backgroundColor = widget.backgroundColor;
    this._backgroundImage = widget.backgroundImage;
    this._phoneNumberLogin = widget.phoneNumberLogin;
    this._socialLogin = widget.socialLogin;
    if (_socialLogin == null) {
      _socialLogin = SocialLogin();
    }
    if (_phoneNumberLogin != null) {
      _showPhoneLogin = true;
    } else {
      _showPhoneLogin = false;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: _backgroundColor,
        body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(image: _backgroundImage),
            padding: EdgeInsets.all(40),
            child: Column(
              children: [
                SizedBox(
                  height: 70,
                ),
                getLogoImage(_logoImage),
                getLogoTagLine(_logoTagLine),
                Expanded(
                  child: getLoginWidget(),
                ),
                getSwitchButton()
              ],
            )));
  }

  Text getLogoTagLine(Text _logoTagLine) {
    if (_logoTagLine == null) {
      return Text('');
    } else {
      return _logoTagLine;
    }
  }

  Widget getLogoImage(Image _logoImage) {
    if (_logoImage == null) {
      return Container();
    } else {
      return _logoImage;
    }
  }

  Widget getLoginWidget() {
    if (_showPhoneLogin == true) {
      return _phoneNumberLogin;
    } else {
      return _socialLogin;
    }
  }

  Widget getSwitchButton() {
    if (_showPhoneLogin == true) {
      return Expanded(
        child: Align(
          alignment: Alignment.bottomCenter,
          child: TextButton(
              child: Text("Other SignIn Method"),
              onPressed: () {
                setState(() {
                  if (_socialLogin != null) {
                    _showPhoneLogin = false;
                  }
                });
              }),
        ),
      );
    } else {
      return IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            setState(() {
              if (_phoneNumberLogin != null) {
                _showPhoneLogin = true;
              } else {
                Navigator.of(context).pop();
              }
            });
          });
    }
  }
}
