import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:social_login_template/src/auth/social_enum.dart';

import '../../../social_login_template.dart';

class SocialSignInButton extends StatelessWidget {
  final Color backgroundColor;
  final Image buttonImage;
  final Text buttonText;
  final SocialType loginType;
  final Function callbackFunction;
  SocialSignInButton(
      {this.loginType,
      this.backgroundColor,
      this.buttonImage,
      this.buttonText,
      this.callbackFunction});

  @override
  Widget build(BuildContext context) {

    emptyCallbackFunction(){
      print('do nothing');
    }

    getSignInMethod(SocialType loginType, Function callbackFunction) {
      if (loginType == SocialType.google) {
        callbackFunction();
      }
      if (loginType == SocialType.apple) {
        callbackFunction();
      }
      if (loginType == SocialType.facebook) {
        callbackFunction();
      }
      if (loginType == SocialType.email) {
        if(callbackFunction == null){
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => EmailLogin(emptyCallbackFunction())));
        }else{
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => EmailLogin(callbackFunction())));
        }
      }
    }



    if (loginType == SocialType.apple && Platform.isAndroid) {
      return Container();
    } else {
      return Padding(
        padding: const EdgeInsets.all(0),
        child: OutlinedButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(backgroundColor),
              shape: MaterialStateProperty.all(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(40),
                ),
              ),
            ),
            onPressed: () {
              getSignInMethod(loginType, callbackFunction);
            },
            child: signInButton(
              buttonImage,
              buttonText,
            )),
      );
    }
  }

  Widget signInButton(Image image, Text signInText) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: Row(
        //mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          getImage(image),
          Padding(
              padding: const EdgeInsets.only(left: 10),
              child: getSignInText(signInText))
        ],
      ),
    );
  }

  Widget getImage(Image image) {
    if (image != null) {
      return image;
    } else {
      return Container();
    }
  }

  Widget getSignInText(Text signInText) {
    if (signInText != null) {
      return signInText;
    } else {
      return Text('');
    }
  }
}
