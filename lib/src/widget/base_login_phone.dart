import 'package:flutter/material.dart';
import 'package:social_login_template/src/auth/auth_helper.dart';
import 'base_login.dart';
import 'phone_login_widget/login_phone_verification.dart';

class PhoneNumberLogin extends StatefulWidget {
  final TextEditingController phoneNumberController = TextEditingController();
  final InputDecoration phoneNumberInput;
  final ButtonStyle verifyButtonStyle;
  final Widget verifyButtonChild;
  final Function callbackFunction;
  PhoneNumberLogin({
    this.phoneNumberInput,
    this.verifyButtonStyle,
    this.verifyButtonChild,
    this.callbackFunction,
  });
  @override
  _PhoneNumberLoginState createState() => _PhoneNumberLoginState();
}

class _PhoneNumberLoginState extends State<PhoneNumberLogin> {
  TextEditingController _phoneNumberController;
  InputDecoration _phoneNumberInput;
  ButtonStyle _verifyButtonStyle;
  Widget _verifyButtonChild;
  Function _callbackFunction;

  @override
  void initState() {
    this._phoneNumberController = widget.phoneNumberController;
    this._phoneNumberInput = widget.phoneNumberInput;
    this._verifyButtonStyle = widget.verifyButtonStyle;
    this._verifyButtonChild = widget.verifyButtonChild;
    this._callbackFunction = widget.callbackFunction;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        getPhoneNumberFormField(_phoneNumberInput),
        getVerifyNumber(context, _verifyButtonStyle, _verifyButtonChild),
        // ElevatedButton(
        //     child: Text('Back'),
        //     onPressed: () {
        //       Navigator.of(context).pop();
        //     })
      ],
    );
  }

  TextFormField getPhoneNumberFormField(InputDecoration numberInput) {
    if (numberInput == null) {
      return TextFormField(
          controller: _phoneNumberController,
          decoration: InputDecoration(
              labelText: 'Phone number (+Country Code xxxxxxxxxx)'));
    } else {
      return TextFormField(
          controller: _phoneNumberController, decoration: numberInput);
    }
  }

  Container getVerifyNumber(BuildContext context,
      ButtonStyle _verifyButtonStyle, Widget _verifyButtonChild) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      alignment: Alignment.center,
      child: ElevatedButton(
        style: _verifyButtonStyle,
        child: getVerifyButtonChild(_verifyButtonChild),
        onPressed: () {
          // AuthHelper.verifyPhoneNumber(_phoneNumberController.text)
          //     .then((value) {
          //   switchToVerifyPhoneNumber(context);
          // }).catchError((error) {
          //   ScaffoldMessenger.of(context)
          //       .showSnackBar(BaseLogin.showSnackBar('Failed to send sms'));
          // });
          _callbackFunction();
        },
      ),
    );
  }

  void switchToVerifyPhoneNumber(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => MyPhoneVerification()));
  }

  Widget getVerifyButtonChild(Widget verifyButtonChild) {
    if (verifyButtonChild == null) {
      return Text('Verify');
    } else {
      return verifyButtonChild;
    }
  }
}
