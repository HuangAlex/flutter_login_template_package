import 'package:flutter/material.dart';
import 'package:social_login_template/src/auth/social_enum.dart';
import 'package:social_login_template/src/widget/social_login_widget/login_button.dart';

import 'social_login_list.dart';

class SocialLogin extends StatefulWidget {
  final List<Widget> socialLoginWidgets;
  SocialLogin({this.socialLoginWidgets});
  @override
  _SocialLoginState createState() => _SocialLoginState();
}

class _SocialLoginState extends State<SocialLogin> {
  List<Widget> _socialLoginWidgets;
  @override
  void initState() {
    if(_socialLoginWidgets != null){
      this._socialLoginWidgets = widget.socialLoginWidgets;
    } else {
      _socialLoginWidgets = [
        SocialSignInButton(backgroundColor: Colors.white,
          buttonImage: Image(image: AssetImage('assets/images/google_logo.png', package: 'social_login_template'), height: 16,),
          buttonText: Text('SignIn With Google', 
          style: TextStyle(
                fontSize: 12,
                color: Colors.black54,
                fontWeight: FontWeight.w600,
              ),),
          loginType: SocialType.google
        ),
        SocialSignInButton(backgroundColor: Colors.white,
          buttonImage: Image(image: AssetImage('assets/images/facebook_logo.png', package: 'social_login_template'), height: 16,),
          buttonText: Text('SignIn With Facebook', style: TextStyle(
                fontSize: 12,
                color: Colors.black54,
                fontWeight: FontWeight.w600,
              ),),
          loginType: SocialType.facebook,
        ),
        SocialSignInButton(backgroundColor: Colors.black,
          buttonImage: Image(image: AssetImage('assets/images/apple_logo.png', package: 'social_login_template'), height: 16,),
          buttonText: Text('SignIn With Apple', style: TextStyle(
                fontSize: 12,
                color: Colors.white,
                fontWeight: FontWeight.w600,
              ),), 
          loginType: SocialType.apple,
        ),
        SocialSignInButton(backgroundColor: Colors.white,
          buttonImage: Image(image: AssetImage('assets/images/moby_logo.png', package: 'social_login_template'), height: 16,),
          buttonText: Text('SignIn With Email', style: TextStyle(
                fontSize: 12,
                color: Colors.black54,
                fontWeight: FontWeight.w600,
              ),), 
          loginType: SocialType.email,
        )
      ];
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        MySocialWidgets(
            width: 200,
            height: 400,
            loginWidgets: _socialLoginWidgets,
            scrollDir: Axis.vertical),
      ],
    );
  }
}
