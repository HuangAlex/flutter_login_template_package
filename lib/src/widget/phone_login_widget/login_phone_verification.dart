import 'package:flutter/material.dart';
import 'package:social_login_template/src/auth/auth_helper.dart';

class MyPhoneVerification extends StatefulWidget {
  @override
  _MyPhoneVerificationState createState() => _MyPhoneVerificationState();
}

class _MyPhoneVerificationState extends State<MyPhoneVerification> {
  final TextEditingController _smsController = TextEditingController();
  // final SmsAutoFill _autoFill = SmsAutoFill();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  SnackBar showSnackBar(String title) {
    final snackbar = SnackBar(
      content: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 15),
      ),
    );
    return snackbar;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          backgroundColor: Colors.blue,
          centerTitle: true,
          title: Text('Phone Verification'),
        ),
        body: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              TextFormField(
                controller: _smsController,
                decoration:
                    const InputDecoration(labelText: 'Verification code'),
              ),
              Container(
                padding: const EdgeInsets.only(top: 16.0),
                alignment: Alignment.center,
                child: ElevatedButton(
                    onPressed: () async {
                      AuthHelper.signInWithPhoneNumber(_smsController.text)
                          .then((value) {
                        ScaffoldMessenger.of(context)
                            .showSnackBar(showSnackBar('Sign in Success'));
                      }).catchError((error) {
                        ScaffoldMessenger.of(context)
                            .showSnackBar(showSnackBar(error.toString()));
                      });
                    },
                    child: Text("Sign in")),
              ),
            ],
          ),
        ));
  }
}
