import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'email_login.dart';
class EmailRegister extends StatefulWidget {
  final Function callbackFunction;
  EmailRegister(this.callbackFunction);
  @override
  _EmailRegisterState createState() => _EmailRegisterState();
}

class _EmailRegisterState extends State<EmailRegister> {
  Function _callbackFunction;

  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  SnackBar showSnackBar(String title) {
    final snackbar = SnackBar(
      content: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 15),
      ),
    );
    return snackbar;
  }

  var emailController = TextEditingController();

  var passwordController = TextEditingController();

  @override
  void initState() {
    this._callbackFunction = widget.callbackFunction;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 70,
                ),
                Image(
                  alignment: Alignment.center,
                  height: 100.0,
                  width: 100.0,
                  image: AssetImage("assets/images/moby_logo.png"),
                ),
                SizedBox(
                  height: 40,
                ),
                Text(
                  'Sign Up with Email',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline5,
                ),
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Column(
                    children: <Widget>[
                      // Email Address
                      TextField(
                        controller: emailController,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          labelText: 'Email Address',
                          labelStyle: TextStyle(
                            fontSize: 14.0,
                          ),
                          hintStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 10.0,
                          ),
                        ),
                        style: TextStyle(fontSize: 14),
                      ),

                      SizedBox(
                        height: 10,
                      ),

                      // Password
                      TextField(
                        controller: passwordController,
                        obscureText: true,
                        decoration: InputDecoration(
                          labelText: 'Password',
                          labelStyle: TextStyle(
                            fontSize: 14.0,
                          ),
                          hintStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 10.0,
                          ),
                        ),
                        style: TextStyle(fontSize: 14),
                      ),

                      SizedBox(
                        height: 40,
                      ),
                    ],
                  ),
                ),
                TextButton(
                    onPressed: () {
                      if (!emailController.text.contains('@')) {
                        ScaffoldMessenger.of(context).showSnackBar(showSnackBar(
                            'Please provide a valid email address.'));
                        return;
                      }

                      if (passwordController.text.length < 8) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            showSnackBar('Please provide a valid password.'));
                        return;
                      }
                      _callbackFunction();
                      // AuthHelper.registerWithEmailAndPassword(
                      //         emailController.text, passwordController.text)
                      //     .then((value) {
                      //   _onSuccess();
                      // }).catchError((error) {
                      //   _onError(error);
                      // });
                    },
                    child: Text('SIGN UP')),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => EmailLogin(_callbackFunction)));
                  },
                  child: Text('Already have account? Login'),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
