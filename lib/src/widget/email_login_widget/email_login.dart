import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'email_register.dart';

class EmailLogin extends StatefulWidget {
  final Function callbackFunction;
  EmailLogin(this.callbackFunction);
  @override
  _EmailLoginState createState() => _EmailLoginState();
}

class _EmailLoginState extends State<EmailLogin> {
  Function _callbackFunction;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  SnackBar showSnackBar(String title) {
    final snackbar = SnackBar(
      content: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 15),
      ),
    );
    return snackbar;
  }

  var emailController = TextEditingController();

  var passwordController = TextEditingController();

  @override
  void initState() {
    this._callbackFunction = widget.callbackFunction;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 70,
                ),
                Image(
                  alignment: Alignment.center,
                  height: 100.0,
                  width: 100.0,
                  image: AssetImage("assets/images/moby_logo.png",package: 'social_login_template'),
                ),
                SizedBox(
                  height: 40,
                ),
                Text(
                  'Login with Email',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline5,
                ),
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Column(
                    children: <Widget>[
                      // Email Address
                      TextField(
                        controller: emailController,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          labelText: 'Email Address',
                          labelStyle: TextStyle(
                            fontSize: 14.0,
                          ),
                          hintStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 10.0,
                          ),
                        ),
                        style: TextStyle(fontSize: 14),
                      ),

                      SizedBox(
                        height: 10,
                      ),

                      // Password
                      TextField(
                        controller: passwordController,
                        obscureText: true,
                        decoration: InputDecoration(
                          labelText: 'Password',
                          labelStyle: TextStyle(
                            fontSize: 14.0,
                          ),
                          hintStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 10.0,
                          ),
                        ),
                        style: TextStyle(fontSize: 14),
                      ),

                      SizedBox(
                        height: 40,
                      ),
                    ],
                  ),
                ),
                TextButton(
                    onPressed: () {
                      if (!emailController.text.contains('@')) {
                        ScaffoldMessenger.of(context).showSnackBar(showSnackBar(
                            'Please provide a valid email address.'));
                        return;
                      }

                      if (passwordController.text.length < 8) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            showSnackBar('Please provide a valid password.'));
                        return;
                      }
                      _callbackFunction();
                      // AuthHelper.signInWithEmailAndPassword(
                      //         emailController.text, passwordController.text)
                      //     .then((result) {
                      //   _onSuccess();
                      // }).catchError((error) {
                      //   if (error.toString() == 'email-not-verified') {
                      //     _onFailed();
                      //   } else {
                      //     _onError(error);
                      //   }
                      // });
                    },
                    child: Text('LOGIN')),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => EmailRegister(_callbackFunction)));
                  },
                  child: Text('Don\'t have account? Register'),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
